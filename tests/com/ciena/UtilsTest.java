package com.ciena;

import org.apache.log4j.Logger;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.Random;

import static org.testng.Assert.assertEquals;

/**
 * Created by user on 7/6/15.
 */
public class UtilsTest {

    private Integer a;
    private Integer b;

    private static Logger log = Logger.getLogger(UtilsTest.class);



    @BeforeGroups("math")
    public void beforeGroups() throws IOException {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @AfterGroups("math")
    public void afterGroups() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @BeforeClass
    public void beforeClass() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @AfterClass
    public void afterClass() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @BeforeMethod
    public void beforeMethod() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        Random rand = new Random();
        a = rand.nextInt(100);
        b = rand.nextInt(100);
        log.debug("  generated: " + a + ", " + b);
    }

    @AfterMethod
    public void afterMethod() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        a = null;
        b = null;
    }

    @Test(groups = "math")
    public void testAdd() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        log.debug("  params: " + a + ", " + b);
        assertEquals(Utils.add(a, b), (a + b));
    }

    @Test(groups = "math")
    public void testSubtract() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        log.debug("  params: " + a + ", " + b);
        assertEquals(Utils.subtract(a, b), (a - b));
    }

    @Test(enabled = false)
    public void skippedTest() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void testException() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        int i = 1 / 0;
    }

    @Test(expectedExceptions = ArithmeticException.class)
    public void testException2() {
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        int i = 1 / 1;
    }

    @Test(timeOut = 3000)
    public void testInfiniteLoop() throws Exception{
        log.debug(Thread.currentThread().getStackTrace()[1].getMethodName());
        Utils.infiniteLoop();
    }


}