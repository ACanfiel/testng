package com.ciena;

/**
 * Created by user on 7/6/15.
 */
public class Utils {

    public static int add(int a, int b) {
        return a + b;
    }

    public static int subtract(int a, int b) {
        return a - b;
    }

    public static void infiniteLoop() throws Exception {
        Thread.sleep(10000);
    }
}
